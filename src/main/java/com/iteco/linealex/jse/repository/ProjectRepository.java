package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.entity.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProjectRepository {

    private List<Project> projects = new ArrayList<>();
    private Scanner scanner;

    public ProjectRepository(Scanner scanner) {
        this.scanner = scanner;
    }

    public Project createProject() {
        System.out.println("ENTER NAME: ");
        Project project = new Project();
        project.setName(scanner.nextLine());
        projects.add(project);
        System.out.println("[OK]\n");
        return project;
    }

    public void listAllProjects() {
        System.out.println("[PROJECT LIST]");
        for (int index = 0; index < projects.size(); index++) {
            System.out.println((index+1) + ". " + projects.get(index));
        }
        System.out.println("");
    }

    public String removeProject(String projectName) {
        for (Project project : projects) {
            if(project.getName().equals(projectName)){
                projects.remove(project);
                System.out.println("[OK]\n");
                return project.getId();
            }
        }
        System.out.println("THERE IS NOT SUCH PROJECT! PLEASE TRY AGAIN!\n");
        return null;
    }

    public void removeAllProjects() {
        projects.clear();
        System.out.println("[All PROJECTS REMOVED]\n");
    }

    public Project getProjectViaProjectId(String projectId){
        for (Project project : projects) {
            if(project.getId().equals(projectId)){
                return project;
            }
        }
        return null;
    }

    public Project getProjectViaProjectName(String projectName){
        for (Project project : projects) {
            if(project.getName().equals(projectName)){
                return project;
            }
        }
        return null;
    }

    public List<Project> getProjects() {
        return projects;
    }

}
