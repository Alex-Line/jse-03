package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Task;

import java.util.*;

public class TaskRepository {

    private List<Task> tasks = new ArrayList<>();
    private ProjectRepository projectRepository;
    private Scanner scanner;

    public TaskRepository(ProjectRepository projectRepository, Scanner scanner) {
        this.projectRepository = projectRepository;
        this.scanner = scanner;
    }

    public Task createTask(String projectId) {
        System.out.println("ENTER NAME: ");
        Task currentTask = new Task();
        currentTask.setName(scanner.nextLine());
        currentTask.setProjectId(projectId);
        tasks.add(currentTask);
        System.out.println("[OK]\n");
        return currentTask;
    }

    public boolean stickTaskToProject(String selectedTaskId) {
        System.out.println("ENTER PROJECT NAME: ");
        String projectName = scanner.nextLine();
        for (Project project : projectRepository.getProjects()) {
            if(project.getName().equals(projectName)){
                getTaskViaId(selectedTaskId).setProjectId(project.getId());
                System.out.println("[TASK ATTACHED TO PROJECT: " + projectName + "]\n");
                return true;
            }
        }
        System.out.println("THERE IS NOT SUCH PROJECT! PLEASE TRY AGAIN!\n");
        return false;
    }

    public void showAllTaskFromProject(String selectedProjectId){
        Project project = projectRepository.getProjectViaProjectId(selectedProjectId);
        int index = 1;
        System.out.println("[TASK LIST FROM PROJECT "+ project.getName() +"]");
        for (Task task : tasks) {
            if(task.getProjectId().equals(selectedProjectId)){
                System.out.println(index + ". " + task);
                index++;
            }
        }
        System.out.println();
    }

    public void listAllTasks(String selectedProjectId) {
        System.out.println("[TASK LIST FOR PROJECT ID: " + selectedProjectId + "]");
        int index = 1;
        for (Task task : tasks) {
            if(task.getProjectId() != null && task.getProjectId().equals(selectedProjectId)){
                System.out.println(index + ". " + task);
                index++;
            }
        }
        System.out.println();
    }

    public boolean removeTask(String selectedProjectId) {
        System.out.println("REMOVING TASK FROM PROJECT: " + projectRepository.getProjectViaProjectId(selectedProjectId).getName());
        System.out.println("ENTER NAME: ");
        String nameOfTask = scanner.nextLine();
        try {
            Task removingTask = getTaskViaName(nameOfTask);
            if (tasks.contains(removingTask) && removingTask.getProjectId().equals(selectedProjectId)) {
                tasks.remove(removingTask);
                System.out.println("[OK]\n");
                return true;
            }
        } catch (Exception e) {
            System.out.println("THERE IS NOT SUCH TASK! PLEASE TRY AGAIN!\n");
        }
        return false;
    }

    public void removeAllTasks(String selectedProjectId){
        Project selectedProject = projectRepository.getProjectViaProjectId(selectedProjectId);
        System.out.println("REMOVING TASKS FROM PROJECT: " + selectedProject.getName());
        tasks.removeIf(task -> task.getProjectId().equals(selectedProjectId));
        System.out.println("[ALL TASKS REMOVED]\n");
    }

    public void removeAllTaskFromAllProjects(){
        tasks.clear();
        System.out.println("[ALL TASKS FROM ALL PROJECTS ARE REMOVED]\n");
    }

    public Task createTask() {
        System.out.println("ENTER NAME: ");
        Task currentTask = new Task();
        currentTask.setName(scanner.nextLine());
        tasks.add(currentTask);
        System.out.println("[OK]\n");
        return currentTask;
    }

    public Task getTaskViaId(String taskId){
        for (Task task : tasks) {
            if(task.getId().equals(taskId)){
                return task;
            }
        }
        return null;
    }

    public Task getTaskViaName(String name){
        for (Task task : tasks) {
            if(task.getName().equals(name)){
                return task;
            }
        }
        return null;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

}
