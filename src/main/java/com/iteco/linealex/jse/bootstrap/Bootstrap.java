package com.iteco.linealex.jse.bootstrap;

import com.iteco.linealex.jse.enumerate.TerminalCommand;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.repository.ProjectRepository;
import com.iteco.linealex.jse.repository.TaskRepository;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Bootstrap {

    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;
    private String selectedProjectId;
    private String selectedTaskId;
    private static final Scanner scanner = new Scanner(System.in);
    private final String didNotSelectMessage = "You did not select any project! Select any and thy again \n";

    public Bootstrap() {
        this.projectRepository = new ProjectRepository(scanner);
        this.taskRepository = new TaskRepository(projectRepository, scanner);
    }

    private static TerminalCommand getCommandByConsoleExpression(String name){
        for (TerminalCommand terminalCommand : TerminalCommand.values()) {
            if(terminalCommand.getCommand().equals(name)){
                return terminalCommand;
            }
        }
        System.out.println("There is not such command! Please help to new request \n");
        return TerminalCommand.HELP;
    }

    private void executeExit(){
        System.exit(0);
    }

    private void showHelp(){
        try (BufferedReader reader = new BufferedReader(new FileReader("src/main/resources/help.txt" ))) {
            while (reader.ready()){
                System.out.println(reader.readLine());
            }
            System.out.println("");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void selectProject(){
        selectedProjectId = null;
        System.out.println("Enter the name of project for selection: ");
        String nameOfProject = scanner.nextLine().trim();
        for (Project project : projectRepository.getProjects()) {
            if(project.getName().equals(nameOfProject)){
                System.out.println("Was selected: ");
                System.out.println(project + "\n");
                selectedProjectId = project.getId();
            }
        }
        if(selectedProjectId == null){
            System.out.println("There is not such project as " + nameOfProject + ". Please try to select again \n");
        }
    }

    private void selectTask(){
        selectedTaskId = null;
        System.out.println("Enter the name of task for selection: ");
        String nameOfTask = scanner.nextLine().trim();
        for (Task task : taskRepository.getTasks()) {
            if(task.getName().equals(nameOfTask) && task.getProjectId() == null){
                System.out.println("Was selected: ");
                System.out.println(task + "\n");
                selectedTaskId = task.getId();
            }
        }
        if(selectedTaskId == null) {
            System.out.println("There is not such task as " + nameOfTask + ". Please try to select again \n");
        }
    }

    private void createProject(){
        selectedProjectId = projectRepository.createProject().getId();
    }

    private void showAllProject(){
        projectRepository.listAllProjects();
    }

    private void removeProject(){
        System.out.println("ENTER NAME: ");
        String projectName = scanner.nextLine();
        Project project = projectRepository.getProjectViaProjectName(projectName);
        taskRepository.removeAllTasks(project.getId());
        String projectId = projectRepository.removeProject(projectName);
    }

    private void removeAllProjects(){
        for (Project project : projectRepository.getProjects()) {
            taskRepository.removeAllTasks(project.getId());
        }
        projectRepository.removeAllProjects();
    }

    private void showAllProjectTask(){
        if(selectedProjectId != null){
            taskRepository.showAllTaskFromProject(selectedProjectId);
        } else System.out.println(didNotSelectMessage);
    }

    private boolean stickTask(){
        if(selectedTaskId == null){
            System.out.println("[THERE IS NOY ANY SELECTED TASK]");
            return false;
        }
        if(taskRepository.stickTaskToProject(selectedTaskId)){
            return true;
        }
        return false;
    }

    private void createTask(){
        if(selectedProjectId != null){
            selectedTaskId = taskRepository.createTask(selectedProjectId).getId();
        } else selectedTaskId = taskRepository.createTask().getId();

    }

    private void showTask(){
        if(selectedProjectId != null){
            taskRepository.listAllTasks(selectedProjectId);
        } else System.out.println(didNotSelectMessage);
    }

    private void removeTask(){
        if(selectedProjectId != null){
            taskRepository.removeTask(selectedProjectId);
        } else System.out.println(didNotSelectMessage);
    }

    private void removeAllTasksFromProject(){
        if(selectedProjectId != null){
            taskRepository.removeAllTasks(selectedProjectId);
        } else System.out.println(didNotSelectMessage);
    }

    private void removeAllTasks(){
        taskRepository.removeAllTaskFromAllProjects();
        selectedTaskId = null;
    }

    private void clearSelection(){
        selectedProjectId = null;
        selectedTaskId = null;
        System.out.println("[ALL SELECTION WAS CANCELED]");
    }

    public void listenToConsole(){
        System.out.println("*** Welcome to Task Manager ***");
        while (true) {
            switch (getCommandByConsoleExpression(scanner.nextLine().trim().toLowerCase())) {
                case PROJECT_CREATE: createProject();
                    break;
                case PROJECT_LIST: showAllProject();
                    break;
                case PROJECT_REMOVE: removeProject();
                    break;
                case PROJECT_CLEAR: removeAllProjects();
                    break;
                case PROJECT_SHOW_TASK: showAllProjectTask();
                    break;
                case PROJECT_SELECT: selectProject();
                    break;
                case TASK_SELECT: selectTask();
                    break;
                case TASK_STICK: stickTask();
                        break;
                case TASK_CREATE: createTask();
                    break;
                case TASK_LIST: showTask();
                    break;
                case TASK_REMOVE: removeTask();
                    break;
                case TASK_CLEAR: removeAllTasksFromProject();
                    break;
                case TASK_CLEAR_ALL: removeAllTasks();
                    break;
                case CLEAR_SELECTION: clearSelection();
                    break;
                case EXIT: executeExit();
                    break;
                case HELP: showHelp();
                    break;
                default: System.out.println( "UNKNOWN COMMAND! PLEASE TRY AGAIN: \n" );
            }
        }
    }

}