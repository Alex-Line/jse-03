package com.iteco.linealex.jse;

import com.iteco.linealex.jse.bootstrap.Bootstrap;

public class App {

    public static void main(String[] args) {
        Bootstrap controller = new Bootstrap();
        controller.listenToConsole();
    }

}
