# Task Manager JSE-03

## Developer
Aleksandr Linev

E-mail: uranus_123@mail.ru

## Software

* Maven 3.6.3
* Java 1.8
* JRE
* IDE IntelliJ IDEA CE

## Build commands

```
    mvn clean
```
```
    mvn install
```
## Run command
```
    java -jar target/jse-3.0-RELEASE.jar
```
